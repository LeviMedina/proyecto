package compilador_simple;
public class Token {
    
    public Tipos tipo_Entrada;
    public String valor_Entrada;
    
   public Tipos getTipo() {
        return tipo_Entrada;
    }
 
    public void setTipo(Tipos tipo) {
        this.tipo_Entrada = tipo;
    }
 
    public String getValor() {
        return valor_Entrada;
    }
 
    public void setValor(String valor) {
        this.valor_Entrada = valor;
    }

    // Diccionario    
    enum Tipos {
        NUMERO ("[0-9]+"),
        INICIO ("BEGIN"),
        FIN ("END"),
        OPERADOR("[+,-,*,/]"),
        RELACIONALES ("[>=|<=|>|<|=|<>|{|}|[|]|(|)|,|;|.]"),
        RESERVADAS ("program|if|else|for|while|case");
        public final String patron;
        Tipos(String salida) {
            this.patron = salida;
        }
    }   
}
